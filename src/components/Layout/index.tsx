import { FC } from 'react'
import style from './layout.module.css'
import { IChildrenProps } from './types'

const Layout: FC<IChildrenProps> = ({ children }) => {
	return <div className={style['container-main']}>{children}</div>
}

export default Layout
