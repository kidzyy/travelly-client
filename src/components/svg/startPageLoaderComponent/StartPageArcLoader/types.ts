export interface IStartPageArcIconProps {
	strokeOpacity: number
	svgPathLength: number
	progress: number
}
