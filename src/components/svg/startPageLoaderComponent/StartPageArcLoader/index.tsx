import { FC } from 'react'
import { IStartPageArcIconProps } from './types'

const StartPageArcIcon: FC<IStartPageArcIconProps> = ({
	strokeOpacity,
	svgPathLength,
	progress,
}) => (
	<svg width='281' height='27' viewBox='0 0 281 27' fill='none'>
		<path
			d='M279.717 24.997C236.276 9.76867 189.56 1.48706 140.907 1.48706C91.9498 1.48706 44.9543 9.87251 1.28273 25.2833'
			stroke='white'
			strokeLinecap='round'
			strokeWidth='2'
			strokeOpacity={strokeOpacity}
			strokeDasharray={svgPathLength}
			strokeDashoffset={(svgPathLength * (100 + progress)) / 100}
		/>
	</svg>
)

export default StartPageArcIcon
