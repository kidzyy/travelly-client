export interface IStartPagePlainIconProps {
	planeX: number
	planeY: number
	planeRotate: number
}
