import ReactDOM from 'react-dom/client'
import './index.css'
import StartPage from './screens/StartPage/StartScreen'
const root = ReactDOM.createRoot(document.getElementById('root') as HTMLElement)
root.render(
	<>
		<StartPage />
	</>
)
