import { useEffect, useState } from 'react'
import { IPreloaderState } from './types'

const usePreloader = (): IPreloaderState => {
	const [progress, setProgress] = useState<number>(0)
	const [planeX, setPlaneX] = useState<number>(-2)
	const [planeY, setPlaneY] = useState<number>(-25)
	const [planeRotate, setPLaneRotate] = useState<number>(-14)

	useEffect(() => {
		const simulateLoading = () => {
			let currentProgress = 0
			let currentPlaneX = planeX
			let currentPlaneY = planeY
			let rotatePlane = planeRotate

			const targetY = -46 // Целевое значение Y
			const increaseYValue = 0.1 // Значение, на которое увеличиваем Y
			const decreaseYValue = 0.1 // Значение, на которое уменьшаем Y

			const animate = () => {
				currentProgress += 1
				currentPlaneX += 2.5
				rotatePlane += 0.37

				// Рассчитываем разницу между текущим и целевым Y
				const deltaY = targetY - currentPlaneY

				// Если разница больше чем минимальное значение, двигаемся к целевому значению
				if (Math.abs(deltaY) > 0.1 && currentProgress < -targetY) {
					currentPlaneY += deltaY * 0.05 // Используем коэффициент для более плавного перемещения
				} else {
					currentPlaneY -= deltaY * 0.04
				}

				// Увеличиваем или уменьшаем currentPlaneY в зависимости от его значения
				if (currentProgress > -targetY) {
					currentPlaneY += increaseYValue
				} else {
					currentPlaneY -= decreaseYValue
				}

				setProgress(currentProgress)
				setPlaneX(currentPlaneX)
				setPlaneY(currentPlaneY)
				setPLaneRotate(rotatePlane)

				if (currentProgress < 100) {
					requestAnimationFrame(animate)
				}
			}

			requestAnimationFrame(animate)
		}

		simulateLoading()
	}, [])

	return {
		progress,
		planeX,
		planeY,
		planeRotate,
	}
}

export default usePreloader
