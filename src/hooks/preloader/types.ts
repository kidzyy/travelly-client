export interface IPreloaderState {
	progress: number
	planeX: number
	planeY: number
	planeRotate: number
}
