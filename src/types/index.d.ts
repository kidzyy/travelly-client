declare module '*.module.css' {
	const content: Record<string, string>
	export default content
}
declare module '*.svg' {
	import React = require('react')
	export const ReactComponent: any
	const src: string
	export default src
}
