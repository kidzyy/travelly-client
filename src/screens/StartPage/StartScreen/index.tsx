import { ReactComponent as StartPageLogo } from 'assets/icons/logo/logo.svg'
import Layout from 'components/Layout'
import Preloader from './Preloader'
import style from './index.module.css'

const StartScreen = () => {
	return (
		<section className={style['start-page-section']}>
			<Layout>
				<div className={style['startPage-logo']}>
					<StartPageLogo />
					<h1 className={style['startPage-logo-title']}>Travelly</h1>
				</div>
				<Preloader />
			</Layout>
		</section>
	)
}

export default StartScreen
