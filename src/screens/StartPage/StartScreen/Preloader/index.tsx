import StartPageArcIcon from 'components/svg/startPageLoaderComponent/StartPageArcLoader'
import StartPagePlainIcon from 'components/svg/startPageLoaderComponent/StartPagePlainIcon'
import MarkerLight from 'components/ui/dot/MarkerLight'
import usePreloader from 'hooks/preloader/usePreloader'
import style from './index.module.css'

const Preloader = () => {
	const { progress, planeX, planeY, planeRotate } = usePreloader()

	const svgPathLength = 290.92
	const strokeOpacity = 1 + progress / 100 - 1

	return (
		<div className={style['custom-loader']}>
			<MarkerLight styles={{ transform: 'translate(-120%, 150%)' }} />
			<StartPageArcIcon
				strokeOpacity={strokeOpacity}
				svgPathLength={svgPathLength}
				progress={progress}
			/>
			<StartPagePlainIcon
				planeX={planeX}
				planeY={planeY}
				planeRotate={planeRotate}
			/>
			<MarkerLight
				styles={{
					transform: 'translate(120%, -320%)',
					right: 0,
				}}
			/>
		</div>
	)
}

export default Preloader
